﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CStoreSCP
{
    public class Series
    {
        public string Id { get; set; }
        public string Modality { get; set; }
        public string SeriesNumber { get; set; }
        public string Description { get; set; }
        public string StudyId { get; set; }

        public Series() { }

        public void Save()
        {
            try
            {
                SqlConnection sqlConnection =
                    new SqlConnection(
                        "Data Source=desktop-aevob6n;Initial Catalog=PatientDB;Integrated Security=True;User Id=demo;Password=demo1234");
                sqlConnection.Open();
                string sqlstr = "insert into TblSeriesInfo (SeriesInstanceId,StudyId,Modality,SeriesNumber,SeriesDescription) values (@SeriesInstanceId,@StudyId,@Modality,@SeriesNumber,@SeriesDescription)";
                SqlCommand mycom = new SqlCommand(sqlstr, sqlConnection);
                //Add parameters
                mycom.Parameters.Add(new SqlParameter("@SeriesInstanceId", SqlDbType.NChar, 100));
                mycom.Parameters.Add(new SqlParameter("@StudyId", SqlDbType.NChar, 100));
                mycom.Parameters.Add(new SqlParameter("@Modality", SqlDbType.NChar, 100));
                mycom.Parameters.Add(new SqlParameter("@SeriesNumber", SqlDbType.NChar, 100));
                mycom.Parameters.Add(new SqlParameter("@SeriesDescription", SqlDbType.NText));
                //Assign values 
                mycom.Parameters["@SeriesInstanceId"].Value = Id;
                mycom.Parameters["@StudyId"].Value = StudyId;
                mycom.Parameters["@Modality"].Value = Modality;
                mycom.Parameters["@SeriesNumber"].Value = SeriesNumber;
                mycom.Parameters["@SeriesDescription"].Value = Description;
                //Execute 
                mycom.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
