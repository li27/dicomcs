﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CStoreSCP
{
    public class Patient
    {
        public string PatientId { get; set; }

        public string Name { get; set; }

        public string Gender { get; set; }

        public string DOB { get; set; }

        public int Age { get; set; }

        public Patient() { }

        public void Save ()
        {
            try
            {
                SqlConnection sqlConnection =
                    new SqlConnection(
                        "Data Source=desktop-aevob6n;Initial Catalog=PatientDB;Integrated Security=True;User Id=demo;Password=demo1234");
                sqlConnection.Open();
                string sqlstr = "insert into TblPatient (PatientId, Name, DOB, Gender, Age) values (@PatientId,@Name,@DOB,@Gender,@Age)";
                SqlCommand mycom = new SqlCommand(sqlstr, sqlConnection);
                //Add parameters
                mycom.Parameters.Add(new SqlParameter("@PatientId", SqlDbType.NChar, 100));
                mycom.Parameters.Add(new SqlParameter("@Name", SqlDbType.NChar, 50));
                mycom.Parameters.Add(new SqlParameter("@DOB", SqlDbType.NChar, 20));
                mycom.Parameters.Add(new SqlParameter("@Gender", SqlDbType.NChar, 10));
                mycom.Parameters.Add(new SqlParameter("@Age", SqlDbType.Int));
                //Assign values 
                mycom.Parameters["@PatientId"].Value = PatientId;
                mycom.Parameters["@Name"].Value = Name;
                mycom.Parameters["@DOB"].Value = DOB;
                mycom.Parameters["@Gender"].Value = Gender;
                mycom.Parameters["@Age"].Value = Age;
                //Execute 
                mycom.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
