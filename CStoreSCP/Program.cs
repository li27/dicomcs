﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dicom;
using Dicom.Network;
using System.Threading;
using CStoreSCP;
using System.IO;

namespace CStoreSCP
{
    class Program
    {

        static void Main(string[] args)
        {
            //get listening port and storage folder
            Settings settings = new Settings();
            settings.SetSettings();

            //Save file to pre-set storage folder
            CStoreSCPProvider.OnCStoreRequestCallBack = (request) =>
            {
                DicomDataset dt = request.Dataset;
                var studyUid = request.Dataset.Get<string>(DicomTag.StudyInstanceUID);
                var instUid = request.SOPInstanceUID.UID;
                var pathstring = settings.StorageFolder.Trim();
                //var path = Path.GetFullPath(@"c:\Temp");
                var path = Path.GetFullPath(pathstring);
                path = Path.Combine(path, studyUid);
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);
                path = Path.Combine(path, instUid) + ".dcm";
                request.File.Save(path);
                SaveToDb(dt);
                return new DicomCStoreResponse(request, DicomStatus.Success);
            }; 

            //Start SCP service
            var server = new DicomServer<CStoreSCPProvider>(settings.ListeningPort);
            Console.ReadLine();
        }

        //Save file information to ms-sql database
        public static void SaveToDb(DicomDataset dt)
        {
            Patient patient = new Patient();
            patient.Name = dt.Get<string>(DicomTag.PatientName, "");
            patient.PatientId = dt.Get<string>(DicomTag.PatientID, "");
            patient.DOB = dt.Get<string>(DicomTag.PatientBirthDate);
            patient.Gender = dt.Get<string>(DicomTag.PatientSex);
            try { 
                patient.Age = Convert.ToInt16(dt.Get<string>(DicomTag.PatientAge));}
            catch
            {
                patient.Age = 0;
            }
            patient.Save();

            Study study = new Study();
            study.StudyId = dt.Get<string>(DicomTag.StudyID,"");
            study.StudyInstanceId = dt.Get<string>(DicomTag.StudyInstanceUID,"");
            study.Description = dt.Get<string>(DicomTag.StudyDescription);
            study.AccessionNumber = dt.Get<string>(DicomTag.AccessionNumber);
            study.StudyDate = dt.Get<string>(DicomTag.StudyDate);
            study.StudyTime = dt.Get<string>(DicomTag.StudyTime);
            study.PatientId = dt.Get<string>(DicomTag.PatientID, "");
            study.Save();

            Series series = new Series();
            series.Id = dt.Get<string>(DicomTag.SeriesInstanceUID,"");
            series.Modality = dt.Get<string>(DicomTag.Modality);
            series.SeriesNumber = dt.Get<string>(DicomTag.SeriesNumber);
            series.Description = dt.Get<string>(DicomTag.SeriesDescription);
            series.StudyId = dt.Get<string>(DicomTag.StudyID, "");
            series.Save();
        }
    }
}
