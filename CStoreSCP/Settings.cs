﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CStoreSCP
{
    public class Settings
    {
        public int ListeningPort { get; set; }
        public string StorageFolder { get; set; }

        public Settings ()
        {
            ListeningPort = 0;
            StorageFolder = "";
        }

        //Get listening port and storage folder from 
        public void SetSettings ()
        {
            try
            {
                SqlConnection sqlConnection =
                    new SqlConnection(
                        "Data Source=desktop-aevob6n;Initial Catalog=PatientDB;Integrated Security=True;User Id=demo;Password=demo1234");
                sqlConnection.Open();
                string sql = "select * from TblConfiguration";
                DataSet dataSet = new DataSet();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(sql, sqlConnection);
                sqlDataAdapter.Fill(dataSet, "tblConfiguration");
                DataTable tblConfiguration = dataSet.Tables["tblConfiguration"];

                for (int i = 0; i < tblConfiguration.Rows.Count; i++)
                {
                    ListeningPort = tblConfiguration.Rows[i].Field<int>("ListeningPort");
                    StorageFolder = tblConfiguration.Rows[i].Field<string>("StorageFolder");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
