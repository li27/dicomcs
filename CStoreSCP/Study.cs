﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CStoreSCP
{
    public class Study
    {
        public string StudyId { get; set; }

        public string StudyInstanceId { get; set; }

        public string PatientId { get; set; }

        public string Description { get; set; }

        public string AccessionNumber { get; set; }
        //public int Age { get; set; }

        public string StudyDate { get; set; }

        public string StudyTime { get; set; }

        public Study() { }

        public void Save ()
        {
            try
            {
                SqlConnection sqlConnection =
                    new SqlConnection(
                        "Data Source=desktop-aevob6n;Initial Catalog=PatientDB;Integrated Security=True;User Id=demo;Password=demo1234");
                sqlConnection.Open();
                string sqlstr = "insert into TblStudy (StudyId,Description,AccessionNumber,PatientId,StudyDate,StudyTime,StudyInstanceId) values (@StudyId,@Description,@AccessionNumber,@PatientId,@StudyDate,@StudyTime,@StudyInstanceId)";
                SqlCommand mycom = new SqlCommand(sqlstr, sqlConnection);
                //Add parameters
                mycom.Parameters.Add(new SqlParameter("@StudyId", SqlDbType.NChar, 100));
                mycom.Parameters.Add(new SqlParameter("@Description", SqlDbType.NText));
                mycom.Parameters.Add(new SqlParameter("@AccessionNumber", SqlDbType.NChar, 50));
                mycom.Parameters.Add(new SqlParameter("@PatientId", SqlDbType.NChar, 100));
                mycom.Parameters.Add(new SqlParameter("@StudyDate", SqlDbType.NChar, 20));
                mycom.Parameters.Add(new SqlParameter("@StudyTime", SqlDbType.NChar, 20));
                mycom.Parameters.Add(new SqlParameter("@StudyInstanceId", SqlDbType.NChar, 100));
                //Assign values 
                mycom.Parameters["@StudyId"].Value = StudyId;
                mycom.Parameters["@Description"].Value = Description;
                mycom.Parameters["@AccessionNumber"].Value = AccessionNumber;
                mycom.Parameters["@PatientId"].Value = PatientId;
                mycom.Parameters["@StudyDate"].Value = StudyDate;
                mycom.Parameters["@StudyTime"].Value = StudyTime;
                mycom.Parameters["@StudyInstanceId"].Value = StudyId;
                //Execute 
                mycom.ExecuteNonQuery();
                sqlConnection.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
