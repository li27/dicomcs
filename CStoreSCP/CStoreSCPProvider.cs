﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dicom;
using Dicom.Log;
using Dicom.Network;
using System.Threading;
using System.IO;

namespace CStoreSCP
{
    public delegate DicomCStoreResponse OnCStoreRequestCallback(DicomCStoreRequest request);  

    class CStoreSCPProvider : DicomService, IDicomServiceProvider, IDicomCStoreProvider
    {
        //Constructor - old version, not the up-to-date version
        public CStoreSCPProvider(Stream stream, Logger log) : base(stream, log) { }

        public static OnCStoreRequestCallback OnCStoreRequestCallBack;
        public DicomCStoreResponse OnCStoreRequest(DicomCStoreRequest request)
        {
            //Save file to storage folder
            if (OnCStoreRequestCallBack != null)
            {
                return OnCStoreRequestCallBack(request);
            }
            //Save file to somewhere I couldn't find
            return new DicomCStoreResponse(request,DicomStatus.Success);
        }

        public void OnCStoreRequestException(string tempFileName, Exception e)
        {

        }

        public void OnReceiveAssociationRequest(DicomAssociation association)
        {
            foreach (var pc in association.PresentationContexts)
            {
                if (pc.AbstractSyntax == DicomUID.Verification)
                    pc.SetResult(DicomPresentationContextResult.Accept);
                else
                {
                    //pc.SetResult(DicomPresentationContextResult.RejectAbstractSyntaxNotSupported);
                }
                if (pc.AbstractSyntax == DicomUID.CTImageStorage)
                {
                    pc.SetResult(DicomPresentationContextResult.Accept);
                }
            }
            SendAssociationAccept(association);

        }

        public void OnReceiveAssociationReleaseRequest()
        {
            SendAssociationReleaseResponse();

        }

        public void OnReceiveAbort(DicomAbortSource source, DicomAbortReason reason)
        {

        }

        public void OnConnectionClosed(int errorCode)
        {

        }
    }
}
