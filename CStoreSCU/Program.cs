﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dicom;
using Dicom.Network;
using System.Threading;

namespace CStoreSCU
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new DicomClient();
            client.NegotiateAsyncOps();
            //client.AddRequest(new DicomCEchoRequest());
            client.AddRequest(new DicomCStoreRequest(@"test.dcm"));
            //port number 3146
            client.Send("127.0.0.1", 3146, false, "SCU", "ANY-SCP");
            Thread.Sleep(2000000);

        }
    }
}
